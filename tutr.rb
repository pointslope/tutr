require 'cuba'
require "cuba/render"
require "org-ruby"

# Cuba is a microframework built on Rack
# http://soveran.github.io/cuba/
# https://github.com/soveran/cuba

Cuba.plugin Cuba::Render

# Setup Rack Middleware
Cuba.use Rack::Session::Cookie, secret: "646BB65F-862C-4217-82E6-E3C019A436AB"
#Cuba.use Rack::File, root: "public"

Cuba.define do
  on get do
    # /posts
    on "posts" do
      res.write "Hello world!"
    end

    # /posts/2014/01/01/happy-new-year
    on "posts/:y/:m/:d/:slug" do |y, m, d, slug|
      res.write "Happy New Year!" 
    end 

    on root do
      res.redirect "/posts"
    end
  end
end

